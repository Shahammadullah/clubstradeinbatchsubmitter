package com.tgw.tradein.runner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.tgw.tradein.batch.steps.MessageSender;
import com.tgw.tradein.batch.steps.TradeInAmountDAO;


/**
 * Created by Gary.Myers on 10/26/2015.
 */
@Component
public class TradeInRequestRunner implements CommandLineRunner
{
   private static final Logger logger = LoggerFactory.getLogger(TradeInRequestRunner.class);
   @Autowired private TradeInAmountDAO tradeinRequestDAO;
   @Autowired private MessageSender sender;
   
   @Override public void run(String... args) throws Exception
   {

      tradeinRequestDAO.requestSubmissionRetrieval().forEach(i -> {
         try
         {
            logger.debug("Attempting to submit {}", i.getTradeInDetails().getTradeinId());
            sender.sendMessage(i);
            tradeinRequestDAO.updateRequest(i.getTradeInDetails().getTradeinId());
            logger.debug("Successfully submitted {}", i.getTradeInDetails().getTradeinId());
         }
         catch(Exception e)
         {
            logger.error("JMS Exception occurred attempting to post {}", i);
         }

         
      });
   }
}
