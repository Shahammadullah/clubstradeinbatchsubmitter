package com.tgw.tradein.batch.configuration;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by Gary.Myers on 8/3/2015.
 */

@SpringBootApplication
@ComponentScan({"com.tgw.tradein.batch.configuration","com.tgw.tradein.batch.steps", "com.tgw.tradein.runner"})
public class ApplicationConfiguration
{
}