package com.tgw.tradein.batch.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.tgw.tradein.batch.steps.TradeInAmountDAO;

@Component
@EnableTransactionManagement
public class DatabaseConfiguration
{
   @Bean public TradeInAmountDAO tradeinRequestRetrieval(JdbcTemplate jdbc)
   {
      return new TradeInAmountDAO(jdbc);
   }
}
