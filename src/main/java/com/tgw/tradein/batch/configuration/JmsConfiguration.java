package com.tgw.tradein.batch.configuration;

import com.ibm.mq.jms.MQQueue;
import com.ibm.msg.client.jms.JmsConnectionFactory;
import com.ibm.msg.client.jms.JmsFactoryFactory;
import com.ibm.msg.client.wmq.WMQConstants;
import com.tgw.tradein.batch.steps.Sender;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

@Component
public class JmsConfiguration
{
   @Bean
   public ConnectionFactory connectionFactory(@Value("${mq.host}") String mqhost, @Value("${mq.username}") String mqusername, @Value("${mq.password}") String mqpassword, @Value("${mq.channel}") String mqChannel, @Value("${mq.queue.manager}") String mqManager) throws JMSException
   {
      JmsFactoryFactory factoryFactory = JmsFactoryFactory.getInstance(WMQConstants.WMQ_PROVIDER);
      JmsConnectionFactory connectionFactory = factoryFactory.createConnectionFactory();

      // Set the properties
      connectionFactory.setStringProperty(WMQConstants.WMQ_HOST_NAME, mqhost);
      connectionFactory.setStringProperty(WMQConstants.USERID, mqusername);
      connectionFactory.setStringProperty(WMQConstants.PASSWORD, mqpassword);

      connectionFactory.setIntProperty(WMQConstants.WMQ_PORT, 1414);
      connectionFactory.setStringProperty(WMQConstants.WMQ_CHANNEL, mqChannel);
      connectionFactory.setIntProperty(WMQConstants.WMQ_CONNECTION_MODE, WMQConstants.WMQ_CM_CLIENT);
      connectionFactory.setStringProperty(WMQConstants.WMQ_QUEUE_MANAGER, mqManager);

      return connectionFactory;
   }

   @Bean
   public MQQueue senderQueue(@Value("${mq.queue}") String queue) throws JMSException
   {
      return new MQQueue(queue);
   }

   @Bean
   public JmsTemplate SenderJMSTemplate(ConnectionFactory connectionFactory, MQQueue queue)
   {
      JmsTemplate jmsTemplate = new JmsTemplate();

      jmsTemplate.setConnectionFactory(connectionFactory);
      jmsTemplate.setPubSubDomain(false);
      jmsTemplate.setDefaultDestination(queue);

      return jmsTemplate;
   }

   @Bean
   public Sender jmsSender(JmsTemplate jmsTemplate)
   {
      return new Sender(jmsTemplate);
   }
}
