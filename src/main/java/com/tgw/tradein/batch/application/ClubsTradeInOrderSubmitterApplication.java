package com.tgw.tradein.batch.application;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.tgw.tradein.batch.configuration.ApplicationConfiguration;

public class ClubsTradeInOrderSubmitterApplication
{
   private static Logger logger = LoggerFactory.getLogger(ClubsTradeInOrderSubmitterApplication.class);

   @Transactional(isolation=Isolation.READ_COMMITTED)
   public static void main(String[] args)
   {
      logger.info("Running Clubs tradein submitter request");
      try(ConfigurableApplicationContext ac = SpringApplication.run(ApplicationConfiguration.class)){}
      logger.info("Clubs tradein submitter request finished");
   }
}
