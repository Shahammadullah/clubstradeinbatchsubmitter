package com.tgw.tradein.batch.steps;
import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlRootElement;


import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TradeInDetails {

	private int tradeinId;
	private String firstName;
	private String lastName;
	private String email;
	private String phoneNumber;
	private int giftAmount;
	private BigDecimal initialgiftAmount;
	private BigDecimal finalGiftAmount;
	private String clubMfr;
	private String clubName;
	private String address1;
	private String address2;
	private String city;
	private String country;
	private String zip;
	private String state;
	private String orderDate;
	

	public BigDecimal getFinalGiftAmount() {
		return finalGiftAmount;
	}
	public void setFinalGiftAmount(BigDecimal finalGiftAmount) {
		this.finalGiftAmount = finalGiftAmount;
	}
	public String getClubMfr() {
		return clubMfr;
	}
	public void setClubMfr(String clubMfr) {
		this.clubMfr = clubMfr;
	}
	public String getClubName() {
		return clubName;
	}
	public void setClubName(String clubName) {
		this.clubName = clubName;
	}
	
	public String getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public int getTradeinId() {
		return tradeinId;
	}
	public void setTradeinId(int tradeinId) {
		this.tradeinId = tradeinId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public void setGiftStatus(String string) {
		// TODO Auto-generated method stub
	}
	public int getGiftAmount() {
		return giftAmount;
	}
	public void setGiftAmount(int giftAmount) {
		this.giftAmount = giftAmount;
	}
	public BigDecimal getInitialgiftAmount() {
		return initialgiftAmount;
	}
	public void setInitialgiftAmount(BigDecimal initialgiftAmount) {
		this.initialgiftAmount = initialgiftAmount;
	}

}
