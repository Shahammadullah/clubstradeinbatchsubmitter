package com.tgw.tradein.batch.steps;

import java.util.List;
import java.util.Locale;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;


@Component
public class EmailTradeInServiceImpl implements EmailTradeInService{


	@Autowired
	private JavaMailSender mailSender;
	@Autowired
	SpringTemplateEngine templateEngine;
	private static Logger logger=LoggerFactory.getLogger(EmailTradeInServiceImpl.class);
	SimpleMailMessage message=new SimpleMailMessage();
	Locale locale = new Locale("UTF-8");
	Context ctx=new Context(locale);

	
	@Override
	public void sendEmail(List<TradeInDetails> tradeInList,int tradeinId) throws Exception {
		
		ctx.setVariable("tradeinId",tradeinId);
		ctx.setVariable("TradeInClubs", tradeInList);

		MimeMessage mimeMessage=this.mailSender.createMimeMessage();
		MimeMessageHelper message=new MimeMessageHelper(mimeMessage,true,"UTF-8");
		message.setFrom("cs@tgw.com");
		message.setTo(tradeInList.get(0).getEmail());
		message.setSubject("TGW.com -Trade in Review update");
		String htmlContent=this.templateEngine.process("emailMessage",ctx);
		message.setText(htmlContent,true);
		
		try{
			
			logger.info("Sending Email update to customer");
			this.mailSender.send(mimeMessage);
			logger.info(" Email update sent");
		}catch(Exception e){
			throw new Exception(e);
		}
		
	}

}

