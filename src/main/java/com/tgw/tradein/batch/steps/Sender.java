package com.tgw.tradein.batch.steps;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;


import javax.jms.JMSException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import java.io.BufferedWriter;
import java.io.StringWriter;

@Component
public class Sender implements MessageSender
{
private static final Logger logger = LoggerFactory.getLogger(Sender.class);
private static final Object CSV_SEPARATOR = ",";
   @Autowired private JmsTemplate jmsTemplate;

   @Autowired
   public Sender(final JmsTemplate jmsTemplate)
   {
      this.jmsTemplate = jmsTemplate;
   }

   

@Override
public void sendMessage(TradeIn tradein) throws JMSException, JAXBException {

	JAXBContext jaxbContext = JAXBContext.newInstance(TradeIn.class);
	Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	StringWriter sw = new StringWriter();
	jaxbMarshaller.marshal(tradein, sw);
	String xmlString = sw.toString();
    jmsTemplate.convertAndSend(xmlString);
    logger.debug("posted into the queue");
	logger.info(xmlString);
	
}
    
}
