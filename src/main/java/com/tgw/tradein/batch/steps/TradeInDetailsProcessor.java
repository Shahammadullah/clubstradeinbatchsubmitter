package com.tgw.tradein.batch.steps;

import java.io.StringWriter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;


import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;



public class TradeInDetailsProcessor implements  ItemProcessor<TradeIn, String> {
	TradeInDetails tradeDetails;
	@Autowired
	TradeInAmountDAO tradeInAmountDAO;
	
	private static Logger logger=LoggerFactory.getLogger(TradeInDetailsProcessor.class);
	@Override
	public String process(TradeIn item) throws Exception {

		JAXBContext jaxbContext = JAXBContext.newInstance(TradeIn.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		StringWriter sw = new StringWriter();
		jaxbMarshaller.marshal(item, sw);
		String xmlString = sw.toString();
		logger.info(xmlString);
		return xmlString;


	}
}

