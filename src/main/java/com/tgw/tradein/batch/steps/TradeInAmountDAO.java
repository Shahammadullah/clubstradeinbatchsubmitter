package com.tgw.tradein.batch.steps;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.tgw.tradein.runner.TradeInRequestRunner;

public class TradeInAmountDAO
{

	private JdbcTemplate jdbcTemplate;

	@Autowired
	private EmailTradeInService emailUpdate;
	
	public TradeInAmountDAO(JdbcTemplate jdbc) {
		jdbcTemplate = jdbc;
	}

	 private static final Logger logger = LoggerFactory.getLogger(TradeInAmountDAO.class);
	public void sendEmail(int tradeinId) throws Exception{

		String sql="select club_mfr, club_name, tradein_price, tradein_final_price, ship_email from golf_club_tradein_details where tradein_id=:tradeinId and gift_cert_status='A' and tradein_final_price is not null";
		List<TradeInDetails> tradeDetails = jdbcTemplate.query(sql, new RowMapper<TradeInDetails>(){

		@Override
			public TradeInDetails mapRow(ResultSet rs, int rowNum) throws SQLException {
		
			TradeInDetails tradeIn=new TradeInDetails();
			tradeIn.setTradeinId(tradeinId);
			tradeIn.setClubMfr(rs.getString("club_mfr"));
			tradeIn.setEmail(rs.getString("ship_email"));
			tradeIn.setClubName(rs.getString("club_name"));
			tradeIn.setInitialgiftAmount(rs.getBigDecimal("tradein_price").divide(new BigDecimal(100)));
			tradeIn.setFinalGiftAmount(rs.getBigDecimal("tradein_final_price").divide(new BigDecimal(100)));
			return tradeIn;
			}
		},tradeinId);
		
		if(tradeDetails.size() !=0){
			//emailUpdate.sendEmail(tradeDetails, tradeinId);
		}
		

	}

	public List<TradeIn> requestSubmissionRetrieval() {
		String sql = "select tradein_id,ship_first_name,SHIP_LAST_NAME,SHIP_COMPANY,SHIP_ADDRESS_LINE1,SHIP_ADDRESS_LINE2,SHIP_CITY,SHIP_STATE,SHIP_ZIP,SHIP_PHONE,SHIP_EMAIL,sum(tradein_final_price) as Gift_Amount_Total,ORDER_DATE from GOLF_CLUB_TRADEIN_DETAILS  where gift_cert_status='A'  group by tradein_id,ship_first_name,SHIP_LAST_NAME,SHIP_COMPANY,SHIP_ADDRESS_LINE1,SHIP_ADDRESS_LINE2,SHIP_CITY,SHIP_STATE,SHIP_ZIP,SHIP_PHONE,SHIP_EMAIL,ORDER_DATE";
		List<TradeIn> tradeDetails = jdbcTemplate.query(sql, new RowMapper<TradeIn>(){

		@Override
			public TradeIn mapRow(ResultSet rs, int rowNum) throws SQLException {
			TradeInDetails tradeIn=new TradeInDetails();
			tradeIn.setTradeinId(rs.getInt("TRADEIN_ID"));
			tradeIn.setEmail(rs.getString("SHIP_EMAIL"));
			tradeIn.setFirstName(rs.getString("SHIP_FIRST_NAME"));
			tradeIn.setLastName(rs.getString("SHIP_LAST_NAME"));
			tradeIn.setGiftAmount(rs.getInt("GIFT_AMOUNT_TOTAL"));
			tradeIn.setPhoneNumber(rs.getString("SHIP_PHONE"));
			tradeIn.setAddress1(rs.getString("SHIP_ADDRESS_LINE1"));
			tradeIn.setAddress2(rs.getString("SHIP_ADDRESS_LINE2"));
			tradeIn.setCity(rs.getString("SHIP_CITY"));
			tradeIn.setState(rs.getString("SHIP_STATE"));
			tradeIn.setZip(rs.getString("SHIP_ZIP"));
			tradeIn.setOrderDate(rs.getDate("ORDER_DATE").toString());
			TradeIn tradeIn1=new TradeIn();
			tradeIn1.setTradeInDetails(tradeIn);
			return tradeIn1;
			}
		});
		
		return tradeDetails;
		
	}

	public void updateRequest(int tradeinId) {
		String sql="UPDATE GOLF_CLUB_TRADEIN_DETAILS set GIFT_CERT_STATUS='O' where tradein_id=? ";
		int updatedRowCount=jdbcTemplate.update(sql, tradeinId);
		logger.info("Number of Rows Updated "+updatedRowCount);
		
	}
}
