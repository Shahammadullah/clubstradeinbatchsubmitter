package com.tgw.tradein.batch.steps;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import org.springframework.stereotype.Component;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XmlRootElement
@XmlSeeAlso(TradeInDetails.class)
public class TradeIn
{

   private TradeInDetails tradeInDetails = new TradeInDetails();
   public TradeInDetails getTradeInDetails()
   {
      return tradeInDetails;
   }

   public void setTradeInDetails(TradeInDetails tradeInDetails)
   {
      this.tradeInDetails = tradeInDetails;
   }



}


