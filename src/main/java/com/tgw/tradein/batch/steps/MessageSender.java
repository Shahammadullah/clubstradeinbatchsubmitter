package com.tgw.tradein.batch.steps;
import javax.jms.JMSException;
import javax.xml.bind.JAXBException;
public interface MessageSender
{

void sendMessage(TradeIn xRequestCatalog) throws JMSException, JAXBException;

}