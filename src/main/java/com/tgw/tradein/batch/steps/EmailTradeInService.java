package com.tgw.tradein.batch.steps;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
public interface EmailTradeInService {

	void sendEmail(List<TradeInDetails> tt, int tradeinId) throws Exception;

}
